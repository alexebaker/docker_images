all: alpine

.PHONY: alpine
alpine: alpine3.10

alpine3.10: dockerfiles/alpine/Dockerfile
	docker build --no-cache --force-rm --pull \
		--build-arg ALPINE_VERSION=3.10 \
		-f dockerfiles/alpine/Dockerfile \
		-t registry.gitlab.com/alexebaker/docker_images/alpine:latest \
		-t registry.gitlab.com/alexebaker/docker_images/alpine:3.10 .
ifeq ($(PUSH_IMAGE), true)
	docker push registry.gitlab.com/alexebaker/docker_images/alpine
endif
