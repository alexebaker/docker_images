#!/usr/bin/env bash


apk upgrade --no-cache
apk add --no-cache bash su-exec tzdata

exit 0
