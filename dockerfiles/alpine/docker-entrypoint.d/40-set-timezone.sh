#!/usr/bin/env bash


[[ -z $TZ ]] && export TZ=UTC

echo "$TZ" > /etc/timezone
rm /etc/localtime || true
ln -snf /usr/share/zoneinfo/$TZ /etc/localtime
