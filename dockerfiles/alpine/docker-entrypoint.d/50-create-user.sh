#!/usr/bin/env bash


[[ -z $USER_NAME ]] && export USER_NAME=root

if [[ ! $(cat /etc/passwd | grep $USER_NAME) ]] ; then
    [[ -z $USER_HOME ]] && export USER_HOME=/home/$USER_NAME
    mkdir -p $USER_HOME

    [[ -z $USER_SHELL ]] && export USER_SHELL=/bin/bash


    [[ -z $USER_ID ]] && export USER_ID=9001
    [[ -z $GROUP_ID ]] && export GROUP_ID=9001

    addgroup -g $GROUP_ID -S $USER_NAME
    adduser -u $USER_ID -s $USER_SHELL -G $USER_NAME -h $USER_HOME -S $USER_NAME
fi

export USER_ID=$(cat /etc/passwd | grep $USER_NAME | awk -F: '{print $3}')
export GROUP_ID=$(cat /etc/passwd | grep $USER_NAME | awk -F: '{print $4}')
export USER_HOME=$(cat /etc/passwd | grep $USER_NAME | awk -F: '{print $6}')
export USER_SHELL=$(cat /etc/passwd | grep $USER_NAME | awk -F: '{print $7}')

[[ -z $ROOT_PASSWORD_FILE ]] || export ROOT_PASSWORD=$(cat $ROOT_PASSWORD_FILE)
[[ -z $ROOT_PASSWORD ]] && export ROOT_PASSWORD=$(cat /dev/urandom | strings | grep -oE '[a-zA-Z0-9!@#$%^&*()?+]' | head -n 32 | tr -d '\n')
echo -e "$ROOT_PASSWORD\n$ROOT_PASSWORD" | passwd root

if [[ $USER_ID != 0 ]] ; then
    [[ -z $USER_PASSWORD_FILE ]] || export USER_PASSWORD=$(cat $USER_PASSWORD_FILE)
    [[ -z $USER_PASSWORD ]] && export USER_PASSWORD=$(cat /dev/urandom | strings | grep -oE '[a-zA-Z0-9!@#$%^&*()?+]' | head -n 32 | tr -d '\n')
    echo -e "$USER_PASSWORD\n$USER_PASSWORD" | passwd $USER_NAME
fi

chown -R $USER_ID:$GROUP_ID $USER_HOME || true
