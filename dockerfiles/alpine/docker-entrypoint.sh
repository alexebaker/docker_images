#!/usr/bin/env bash


echo
echo "Running entrypoint scripts ..."

set -e

for f in $(ls /etc/docker-entrypoint.d/*.sh) ; do
    echo -n "$(basename $f) ... "
    trap "echo 'fail' && cat /var/log/$(basename $f).log" EXIT
    source $f > /var/log/$(basename $f).log 2>&1
    trap - EXIT
    echo "ok"
done

set +e

for e in $(env | awk -F= '{print $1}' | grep -i password) ; do
    unset $e
done

echo

su-exec $USER_NAME "$@"
